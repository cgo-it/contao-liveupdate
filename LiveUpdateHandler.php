<?php 

namespace cgoIT;

class LiveUpdateHandler extends \System {
	
	/**
	 * Load database object
	 * @param array
	 */
	public function __construct()
	{
		$this->import('Database');
		parent::__construct();
	}
	
	public function getRedirect($referer, $version, $uid, $bup) {
		$referer = base64_decode($referer);
		
		list($refUrl, $refIp) = preg_split('/\|/', $referer);
		$redirect = $refUrl;

		$this->log("Update-Request: refUrl=$refUrl, refIp=$refIp, uid=$uid, version=$version", "LiveUpdate", TL_GENERAL);
		
		$domain = $this->checkUpdateId($uid);
		if ($domain == NULL) {
			$this->log("LiveUpdate: falsche UID=$uid", "LiveUpdate", TL_ERROR);
			$_SESSION['LIVE_UPDATE_ERROR'] = "Leider ist ihre Update-ID ung&uuml;ltig!";
		} else if (strstr($refUrl, $domain) == FALSE) {
			$this->log("LiveUpdate: Aufruf von falscher Domain. Aufrufende Domain=$refUrl; konfigurierte URL=$domain", "LiveUpdate", TL_ERROR);
			$_SESSION['LIVE_UPDATE_ERROR'] = "Leider nutzen sie die ID einer falschen Domain!";
		} else {
			$token = md5(uniqid(mt_rand(), true));
			$newVersion = file_get_contents(TL_ROOT . '/files/live-update/version.txt');
			$this->log("LiveUpdate: Update Domain $domain from Version $version to $newVersion.", "LiveUpdate", TL_GENERAL);
			
			$file = $this->getUpdatePack($newVersion);
			if ($file == NULL) {
				$this->log("LiveUpdate: F&uuml;r die gew&uuml;nschte Version $version ist noch kein Update-Pack hinterlegt!", "LiveUpdate", TL_ERROR);
				$_SESSION['LIVE_UPDATE_ERROR'] = "F&uuml;r die gew&uuml;nschte Version ist noch kein Update-Pack hinterlegt!";
			} else {
				$this->Database->prepare("INSERT INTO tl_liveupdate_requests (tstamp, token, ip, updatepack) VALUES(UNIX_TIMESTAMP(), ?, ?, ?)")
											  ->execute($token, $refIp, $file);
				
				$redirect = preg_replace('/maintenance/', 'maintenance&token='.$token.'&bup=1', $refUrl);
				if ($bup) {
					$redirect .= "&bup=1";
				}
			}
		}
		
		return $redirect;		
	}
	
	public function getUpdatePackForToken($token) {
		$this->log("Updatepack f&uuml;r Token $token angefragt.", "LiveUpdate", TL_GENERAL);
		$objFile = $this->Database->prepare("SELECT updatepack FROM tl_liveupdate_requests WHERE token=? AND downloaded is null")
		                ->limit(1)
									  ->execute($token);
		
		if ($objFile->numRows < 1) {
			header("HTTP/1.0 417 Expectation Failed");
			$this->log("Updatepack wurde bereits heruntergeladen.", "LiveUpdate", TL_ERROR);
			return "Updatepack wurde bereits heruntergeladen.";
		}
		
		$file = file_get_contents(TL_ROOT . "/" . $objFile->updatepack);
		$this->Database->prepare("UPDATE tl_liveupdate_requests set downloaded = now() WHERE token=?")
									  ->execute($token);
		return $file;
	}
	
	private function checkUpdateId($uid) {
		$now = strtotime(date("Y-m-d"));
		
		$datValidFrom = $this->Database->prepare("SELECT value FROM tl_formdata_details WHERE pid IN (SELECT id FROM tl_formdata WHERE form=?) AND ".
				                           "pid=(SELECT pid FROM tl_formdata_details WHERE value=?) AND ".
				                           "ff_name=?")
				        ->limit(1)
						->execute('Live Update - Neue Update-ID', $uid, 'valid_from')
		                ->fetchAssoc();
		$datValidTo = $this->Database->prepare("SELECT value FROM tl_formdata_details WHERE pid IN (SELECT id FROM tl_formdata WHERE form=?) AND ".
				                           "pid=(SELECT pid FROM tl_formdata_details WHERE value=?) AND ".
				                           "ff_name=?")
				        ->limit(1)
						->execute('Live Update - Neue Update-ID', $uid, 'valid_to')
		                ->fetchAssoc();

		$domain = NULL;
		if ($now >= strtotime($datValidFrom['value']) && (strlen($datValidTo['value']) == 0 || $now <= strtotime($datValidTo['value']))) {
			$strObj = $this->Database->prepare("SELECT value FROM tl_formdata_details WHERE pid IN (SELECT id FROM tl_formdata WHERE form=?) AND ".
					"pid=(SELECT pid FROM tl_formdata_details WHERE value=?) AND ".
					"ff_name=?")
					->limit(1)
					->execute('Live Update - Neue Update-ID', $uid, 'domain')
					->fetchAssoc();
			
			$domain = $strObj['value'];
		}
		return $domain;
	}

	private function getUpdatePack($version) {
		$arrFile = $this->Database->prepare("SELECT * FROM tl_formdata_details WHERE pid IN (SELECT id FROM tl_formdata WHERE form=?) AND ".
				                            "pid=(SELECT pid FROM tl_formdata_details WHERE value=?) AND ".
				                            "ff_name=?")
		                ->execute('Live Update - Neues Update-Pack', $version, 'file')
		                ->fetchAssoc();
		$file = $arrFile['value'];
		return $file;
	}
}
?>