<?php
	/**
	 * Initialize the system
	 */
  define('TL_MODE', 'BE');
  require('../system/initialize.php');
	
	require_once 'LiveUpdateHandler.php';
	$liveUpdateHandler = new cgoIT\LiveUpdateHandler();
	$redirect = $liveUpdateHandler->getRedirect($_POST['ref'], $_POST['ver'], $_POST['uid'], $_POST['bup']);
	header ("Location: $redirect");
?>